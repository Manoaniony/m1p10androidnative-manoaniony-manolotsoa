FROM node:14-alpine as builder

WORKDIR /server

COPY package.json /server/package.json
COPY yarn.lock /server

RUN yarn install

COPY . /server

EXPOSE 4000
RUN ["chmod", "+x", "./wait-for.sh"]

ARG MONGO_PORT
ARG MONGO_HOST

CMD ./wait-for.sh ${MONGO_HOST}:${MONGO_PORT} -t 4000 -- echo "mongo is up" && yarn dev