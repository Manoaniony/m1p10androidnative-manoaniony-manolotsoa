class Island {
    constructor({
      nomIle,
      img,
      description
    }) {
        this.nomIle = nomIle;
        this.img = img;
        this.description = description;
      }
    }
    module.exports = { Island };