class Profile {
    constructor({
      firstName,
      lastName,
      address,
      phoneNumber,
      cin,
      createdAt,
      updatedAt
    }) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phoneNumber = phoneNumber;
        this.cin = cin;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
      }
    }
    module.exports = { Profile };