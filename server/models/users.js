class Users {
  constructor(
    {_id,nom,prenom,adresse,cin,telephone,mail,mdp,createdAt,updatedAt,accessToken}) 
  {
    this._id = _id;
    this.nom = nom;
    this.prenom = prenom;
    this.adresse = adresse;
    this.cin = cin;
    this.telephone = telephone;
    this.mail = mail;
    this.mdp = mdp;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
    this.accessToken=accessToken;
  }
}
module.exports = { Users };
  