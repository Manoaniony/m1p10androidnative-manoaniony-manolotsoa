function formatDateyyyymmddhhmm(date){
    let result = null;
    let tab = date.split(' ');
    if(tab.length > 0){
        let ddmmyyyy = tab[0];
        let hhmm = tab[1];
        let tb = ddmmyyyy.split('/');
        let yyyymmdd = null;
        if(tb.length > 0)
            yyyymmdd = tb[2]+"/"+tb[1]+"/"+tb[0];
        result = yyyymmdd + " " + hhmm
    }
    return result;
}

function dateDiffParHeure(d1, d2){
    let date1 = new Date(formatDateyyyymmddhhmm(d1));
    let date2 =  new Date(formatDateyyyymmddhhmm(d2));
    let time_diff = date2.getTime() - date1.getTime();
    let days_Diff = time_diff / (1000 * 3600);
    return days_Diff;
}

function getPeriode (periode){
    let mois = periode.substring(0, 2);
    let annee = periode.substring(2);
    return mois+"/"+annee;
}

function formatDateddyyyy(date){
    let result = null;
    let tab1 = date.split(' ');
    if(tab1.length > 0){
        let tab2 = tab1[0].split('/');
        if(tab2.length > 0)
            result = tab2[1]+"/"+tab2[2];
    }
    return result;
}

function getAllPeriode(liste){
    let result = Array();
    let listeDate = Array();
    let vrai = false;
    if(liste.length > 0){
        liste.forEach(element => {
            if(element.releaseAt !== null){
                listeDate.push(formatDateddyyyy(element.receive));
            }
        });
        if(listeDate.length > 0){
            listeDate.forEach(element => {
                vrai = false;
                result.forEach(list => {
                    if(element === list)
                        vrai = true;
                });
                if(!vrai){
                    result.push(element);
                }
            });
        }
    }
    return result;
}

module.exports = { dateDiffParHeure , formatDateyyyymmddhhmm , getPeriode , getAllPeriode}