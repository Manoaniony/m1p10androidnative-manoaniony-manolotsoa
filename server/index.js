const express = require('express');
const { initConstraintDB } = require("./db/connect");
const routerUsers = require("./routes/usersRoute");
const routerAuth = require("./routes/authRoute");
const routerTest = require("./routes/testRoute");
const cors = require('cors');
const app = express();
const config = require("./config");
const { mongoDb } = config;
require('dotenv').config()

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors({
  origin: '*',
  //optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}));

app.use("/users", routerUsers);
app.use("/auth", routerAuth);
app.use("/test", routerTest);


app.get('/', (req, res) => {
  res.send({ greetings: 'hello  world!' })
});

app.post('/post', (req, res) => {
  res.send({ post: `test post ${JSON.stringify(req.body)}` });
});

//const connectionString = "mongodb://127.0.0.1:27017/";
//myFirstDatabase?appName=mongosh+1.3.1
// let connectionString = "mongodb+srv://admin:admin@tourismemada.fpjubym.mongodb.net/";
// if (process.env.NODE_ENV==="development") {
//   connectionString = `${mongoDb.url}/${mongoDb.databaseName}`;
//   console.log(connectionString);
// }
// else{
//   connectionString = "mongodb+srv://admin:admin@tourismemada.fpjubym.mongodb.net/";
// }
initConstraintDB();
// connect(connectionString, (err) => {
//   if (err) {
//     console.log("Erreur lors de la connexion à la base de données");
//     console.log(err);
//     process.exit(-1);
//   } else {
//     console.log("Connexion avec la base de données établie");
//     console.log("Attente des requêtes au port 3OOO");
//   }
// });
app.listen(process.env.PORT || 3000);