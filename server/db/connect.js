const { MongoClient, Db } = require("mongodb");
const {mongoDb} = require('../config');

let client = null;

function getConnection() {
  try {
    if (process.env.NODE_ENV==="development") {
      return getConnectionByUrl(`${mongoDb.url}/${mongoDb.databaseName}`);
    }
    else{
      // return getConnectionByUrl("mongodb+srv://admin:admin@clustergarage.ud9nxz4.mongodb.net/");
      return getConnectionByUrl("mongodb+srv://admin:admin@tourismemada.fpjubym.mongodb.net/");
    }
  } catch (error) {
    throw error;
  }
}

function getConnectionByUrl(url, callback) {
  return new MongoClient(url);
}

function getDb(client, dbname) {
  try {
    if (client && dbname) {
      return client.db(dbname);
    }
    else{
      throw new Error(`An error occured`);
    }
  } catch (error) {
    throw error;
  }
}

function connect(url, callback) {
  if (client === null) {
    client = new MongoClient(url);

    client.connect((err) => {
      if (err) {
        client = null;
        callback(err);
      } else 
        callback();
    });
  } else
    callback();
}

function initConstraintDB(){
  const client = getConnection();
  // console.log(`getDb(client,"garageDB").collection("users")`);
  // console.log(getDb(client,"garageDB").collection("users"));
  getDb(client,"tourismeDB").collection("users").createIndex( { "email": 1 }, { unique: true } );
  return;
}

function db(dbname) {
  var db = client.db(dbname);
  return db;
}

function fermer() {
  if (client) {
    client.close();
    client = null;
  }
}

module.exports = { connect, client, db, fermer, getConnection, getDb, initConstraintDB };
