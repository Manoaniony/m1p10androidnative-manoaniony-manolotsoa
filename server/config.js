require('dotenv').config()

const config = {
    mongoDb: {
        url: process.env.MONGO_URL,
        databaseName: process.env.DATABASE,
      },
    tokenKey:'azertyuiop'
}

module.exports = config;