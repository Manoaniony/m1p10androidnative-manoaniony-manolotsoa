const {Router} = require("express");
const {
    getAllIsland,
    insertIsland,
    getTransformeBinaireImg
} = require("../controllers/testController");
const router = Router();

router.route("/getAllIsland").get(getAllIsland);
router.route("/getTransformeBinaireImg").get(getTransformeBinaireImg);
router.route("/insertIsland").post(insertIsland);

module.exports = router;