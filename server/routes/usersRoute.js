const express = require("express");
const {
  ajouterUsers,
  getAllUsers,
  getUsersById,
  updateUsers,
  deleteUsers,
} = require("../controllers/usersController");
const router = express.Router();



router.route("/ajoutUsers").post(ajouterUsers);
router.route("/getAllUser").get(getAllUsers);
router.route("/getUsers/:id").get(getUsersById);
router.route("/updateUsers/:id").put(updateUsers);
router.route("/deleteUsers/:id").delete(deleteUsers);

module.exports = router;
