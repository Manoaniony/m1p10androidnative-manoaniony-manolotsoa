const {Router} = require("express");
const {
  login,
  me
} = require("../controllers/authController");
const router = Router();

router.route("/login").post(login);
router.route("/me").get(me);

module.exports = router;
