const { ObjectID } = require("bson");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const { getConnection, getDb } = require("../db/connect");
const { Users } = require("../models/users");
const { Profile } = require("../models/profile");
const { UserRoles } = require("../models/userRoles");
const tokenKey = 'azertyuiop';
const saltRounds = 4;

/*const ajouterUsers = async (req, res) => {
  const client = getConnection();
  try {
    let data = req.body;
    const password = await bcrypt.hash(req.body.password, saltRounds);
    const createdAtProfile = moment().format("DD/MM/YYYY HH:mm");
    let profile = new Profile({
      firstName: data.profile.firstName,
      lastName: data.profile.lastName,
      address: data.profile.address,
      phoneNumber: data.profile.phoneNumber,
      cin: data.profile.cin,
      createdAt: createdAtProfile,
      updatedAt: createdAtProfile
    });

    const createdAtUsers = new Date()
    let users = new Users({
      email: data.email,
      userName: data.userName,
      password,
      profile: profile,
      createdAt: createdAtUsers,
      updatedAt: createdAtUsers,
      usersRoles: data.usersRoles
    });

    const userEntity = getDb(client, "garageDB").collection("users");
    let result = await userEntity
      .insertOne(users).then(res => { return res }).catch(err => { throw err })
      console.log(req.body);
      //return null;
    //return res.status(200).json(result);
    return res.status(200).json(null);
  } catch (error) {
    if (error && !error.index) {
      return res.status(200).json(error);
    }
    return res.status(501).json(error);
  } finally {
    client.close();
  }
};*/

const ajouterUsers = async (req, res) => {
  const client = getConnection();
  try {
    console.log(req.body);
    let data = req.body;
    const password = await bcrypt.hash(data.mdp, saltRounds);
    const createdAtUsers = new Date();
    let users = new Users({
      nom: data.nom,
      prenom: data.prenom,
      adresse: data.adresse,
      cin: data.cin,
      telephone: data.telephone,
      mail: data.mail,
      mdp: password,
      createdAt: createdAtUsers,
      updatedAt: createdAtUsers
    });
    const userEntity = getDb(client, "tourismeDB").collection("users");
    let result = await userEntity
      .insertOne(users);
    message = {
      "type": 2
    }
    return res.status(200).json(message);
  } catch (error) {
    console.log(error);
    if (error && !error.index) {
      return res.status(500).json(error);
    }
    return res.status(501).json(error);
  } finally {
    client.close();
  }
};

const getAllUsers = async (req, res) => {
  try {
    let cursor = client
      .db("garageDB")
      .collection("users")
      .find()
      .sort({ nom: 1 });
    let result = await cursor.toArray();
    if (result.length > 0) {
      res.status(200).json(result);
    } else {
      res.status(204).json({ msg: "Aucun users trouvé" });
    }
  } catch (error) {
    console.log(error);
    return res.status(501).json(error);
  }
};

const getUsersById = async (req, res) => {
  try {
    let id = new ObjectID(req.params.id);
    let cursor = client.db("garageDB").collection("users").find({ _id: id });
    let result = await cursor.toArray();
    if (result.length > 0) {
      res.status(200).json(result[0]);
    } else {
      res.status(204).json({ msg: "Cet users n'existe pas" });
    }
  } catch (error) {
    console.log(error);
    res.status(501).json(error);
  }
};

const updateUsers = async (req, res) => {
  try {
    let data = req.body;
    let id = new ObjectID(req.params.id);
    let profile = new Profile(data.profile.firstName, data.profile.lastName, data.profile.address, data.profile.telephone, data.profile.cin, data.profile.createdAt, new Date().toLocaleDateString());

    /** usersRoles */
    let listeRoles = Array();
    if (data.userRoles.length > 0) {
      data.userRoles.forEach(liste => {
        let roles = new UserRoles(liste);
        listeRoles.push(roles);
      });
    }

    let email = data.email;
    let userName = data.userName;
    let password = data.password;
    let createdAt = data.createdAt;
    let updatedAt = new Date()
    let result = await client
      .db('garageDB')
      .collection("users")
      .updateOne({ _id: id }, { $set: { email, userName, password, profile, createdAt, updatedAt, listeRoles } });

    if (result.modifiedCount === 1) {
      res.status(200).json({ msg: "Modification réussie" });
    } else {
      res.status(404).json({ msg: "Cet users n'existe pas" });
    }
  } catch (error) {
    console.log(error);
    res.status(501).json(error);
  }
};

const updateAccessToken = async (req, res) => {
  let id = new ObjectID(req.params.id);
  const updatedAt = moment().format("DD/MM/YYYY HH:mm");
  // const user = await client.db("garageDB").collection("users").findOne(id);
  const { modifiedCount } = await client
    .db('garageDB')
    .collection("users")
    .updateOne({ _id: id }, { $set: { accessToken: "azertuio", updatedAt } })
  if (modifiedCount) {
    const user = await client.db("garageDB").collection("users").findOne(id);
    res.status(200).json(user);
  }
}

const deleteUsers = async (req, res) => {
  try {
    let id = new ObjectID(req.params.id);
    let result = await client
      .db('garageDB')
      .collection("users")
      .deleteOne({ _id: id });
    if (result.deletedCount === 1) {
      res.status(200).json({ msg: "Suppression réussie" });
    } else {
      res.status(404).json({ msg: "Cet users n'existe pas" });
    }
  } catch (error) {
    console.log(error);

    res.status(501).json(error);
  }
};

module.exports = {
  ajouterUsers,
  getAllUsers,
  getUsersById,
  updateUsers,
  deleteUsers,
  updateAccessToken
};