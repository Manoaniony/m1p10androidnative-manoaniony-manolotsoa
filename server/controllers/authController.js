const { ObjectID } = require("bson");
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const { getConnection, getDb } = require("../db/connect");
const tokenKey = 'azertyuiop';

const me = async (req, res) => {
    const client = getConnection();
    try {
        const userEntity = getDb(client, "garageDB").collection("users");
        const { authorization } = req.headers;
        const tokenParsed = authorization.split(" ");

        const { id } = jwt.verify(tokenParsed[1], tokenKey);
        const userFound = await userEntity.findOne(
            { _id: new ObjectID(id) },
            { projection: { accessToken: 0, password: 0 } }
        );
        if (userFound) {
            return res.status(200).json(userFound);
        }
        return res.status(404).json(null);
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    } finally {
        client.close();
    }
}

const login = async (req, res) => {
    const client = getConnection();
    const userEntity = getDb(client, "tourismeDB").collection("users");
    const updatedAt = moment().format("DD/MM/YYYY HH:mm");
    //const { mail, mdp } = JSON.parse(req.body.data);
    try {
        let data = req.body;
        let mail = data.mail;
        let mdp = data.mdp;
        console.log(`mail ${mail} et mdp ${mdp}`)
        const errors = {
            "type": 0,
            "data": "Erreur"
        };
        const userFound = await userEntity.findOne({ mail });
        if (userFound) {
            const validPassword = await bcrypt.compare(mdp, userFound.mdp);
            //const validPassword = mdp;
            if (validPassword) {
                const token = jwt.sign({
                    _id: new ObjectID(userFound._id),
                    email: userFound.email,
                    nom: userFound.nom,
                    prenom: userFound.prenom,
                }, tokenKey, { expiresIn: '23 hours' });
                const { value } = await userEntity.findOneAndUpdate({ _id: userFound._id }, { $set: { accessToken: token, updatedAt } }, { projection: { mdp: 0 } })
                const message = {
                    "type": 1,
                    "data": value
                };
                return res.status(200).json(message);
            }
            else {
                return res.status(403).json(errors)
            }
        }
        return res.status(403).json(errors);
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    } finally {
        client.close();
    }
}

module.exports = { login, me }