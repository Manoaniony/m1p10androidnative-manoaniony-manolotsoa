const { ObjectID } = require("bson");
const fs = require('fs');
const path = require('path');
const { getConnection, getDb } = require("../db/connect");
const { Island } = require("../models/island");
const tokenKey = 'azertyuiop';
const saltRounds = 4;



const getAllIsland = async (req, res) => {
    const client = getConnection();
    try {
        const userEntity = getDb(client, "tourismeDB").collection("island").find();
        let result = await userEntity.toArray();
        if (result.length > 0) {
            message = {
                "type" : 1,
                "data" : result
            }
            res.status(200).json(message);
        } else {
            res.status(204).json({ msg: "Aucun users trouvé" });
    }
    } catch (error) {
        console.log(error);
        return res.status(501).json(error);
    }
  };

  const insertIsland = async (req, res) => {
    const client = getConnection();
    try {
        //let data = JSON.parse(req.body.data);
        let data = req.body.data;
        let island = new Island({
            nomIle : data.nomIle,
            img : data.img,
            description : data.description
          });
        const islandEntity = getDb(client, "tourismeDB").collection("island");
        let result = await islandEntity
        .insertOne(island).then(res => {
            message = {
                "type" : 2,
                "data" : res
            }
            return res.status(200).JSON(message); 
        }).catch(err => { throw err });
        return res.status(200).JSON(result); 
    } catch (error) {
        //console.log(error);
        return res.status(501).json(error);
    }
  };

  const getTransformeBinaireImg = async (req, res) => {
    try {
        console.log(req.body.data);
        try {
            let data = req.body.data;

            const imageData = fs.readFileSync(data.imgURL);
            const imageBytes = Array.from(imageData);
            //const imageData = fs.readFileSync(data.imgURL);
            //const binaryData = Buffer.from(imageData).toString('base64');
            return res.status(200).send(imageBytes);
          } catch (error) {
            console.error('Erreur lors de la conversion de l\'image en binaire :', error);
            res.status(500).send('Une erreur est survenue.');
          }
    } catch (error) {
        console.log(error);
        return res.status(501).json(error);
    }
  };
  

  module.exports = {
    getAllIsland,
    insertIsland,
    getTransformeBinaireImg
  };