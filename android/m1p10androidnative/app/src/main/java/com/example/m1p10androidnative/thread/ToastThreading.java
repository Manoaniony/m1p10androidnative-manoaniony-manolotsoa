package com.example.m1p10androidnative.thread;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.example.m1p10androidnative.fetcher.OnDataFetchedListener;

public class ToastThreading extends Thread {
    private Context context;
    private String message;

    public ToastThreading(Context context) {
        this.context = context;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void run() {
        // Perform background work here

        // Switch to the main UI thread to display the toast
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
