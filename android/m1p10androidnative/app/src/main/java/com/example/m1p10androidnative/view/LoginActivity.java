package com.example.m1p10androidnative.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.health.SystemHealthManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.m1p10androidnative.R;
import com.example.m1p10androidnative.application.MyApplication;
import com.example.m1p10androidnative.constants.Configuration;
import com.example.m1p10androidnative.controller.Control;
import com.example.m1p10androidnative.fetcher.FetchData;
import com.example.m1p10androidnative.fetcher.OnDataFetchedListener;
import com.example.m1p10androidnative.provider.TokenProvider;
import com.example.m1p10androidnative.token.TokenManager;
import com.example.m1p10androidnative.utils.SharedPreferencesUtil;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences sharedPreferences = getSharedPreferences("NomDePreferences", Context.MODE_PRIVATE);
        init();
    }

    private EditText txtMail;
    private EditText txtMdp;
    private Control control;

    private TokenManager tokenManager;

    private void init() {
        txtMail = (EditText) findViewById(R.id.idMail);
        txtMdp = (EditText) findViewById(R.id.idMdp);
        seConnecter();
        sInscrire();
    }

    private void seConnecter() {
        Button buttonConnexion = findViewById(R.id.btnCon);
        buttonConnexion.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String mail = null;
                String mdp = null;
                try {
                    mail = txtMail.getText().toString();
                    mdp = txtMdp.getText().toString();

                    JSONObject body = new JSONObject();
                    body.put("mail", mail);
                    body.put("mdp", mdp);
                    /*<login*/
                    FetchData.makePostRequest(Configuration.getBaseUrl() + "/auth/login", body, new OnDataFetchedListener() {
                        @Override
                        public void onDataFetched(JSONObject data) {
                            try {
                                JSONObject dataUser = data.getJSONObject("data");
                                String token = dataUser.getString("accessToken");
                                MyApplication myApplication = (MyApplication) getApplication();
                                TokenProvider tokenProvider = new TokenProvider(myApplication);
                                tokenProvider.setTokenAuth(token);

                                SharedPreferencesUtil.writeString(LoginActivity.this, "mail", dataUser.getString("mail"));
                                SharedPreferencesUtil.writeString(LoginActivity.this, "nom", dataUser.getString("nom"));
                                SharedPreferencesUtil.writeString(LoginActivity.this, "prenom", dataUser.getString("prenom"));

                                System.out.println("token obtained: " + tokenProvider.getTokenAuth());
                                Intent intent = new Intent(LoginActivity.this, LoadingActivity.class);
                                startActivity(intent);
                                finish();
                            } catch (JSONException e) {
                                throw new RuntimeException(e);
                            }
                        }

                        @Override
                        public void onError(String errorMessage) {
                            // Handle error, display error message, etc.
                            System.out.println("errorMessage: " + errorMessage);
                            Toast.makeText(LoginActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
//                            throw new RuntimeException(errorMessage);
                        }
                    });
                    /*login/>*/
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void sInscrire() {
        ((TextView) findViewById(R.id.txtInsc)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(LoginActivity.this, SignInActivity.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}