package com.example.m1p10androidnative.fetcher;

import org.json.JSONObject;

public interface OnDataFetchedListener {
    void onDataFetched(JSONObject data);

    void onError(String errorMessage);

}
