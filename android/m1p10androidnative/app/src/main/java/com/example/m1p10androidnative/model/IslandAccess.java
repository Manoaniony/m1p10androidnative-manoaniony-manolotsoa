package com.example.m1p10androidnative.model;

import android.content.Context;
import android.util.Log;

import com.example.m1p10androidnative.constants.Configuration;
import com.example.m1p10androidnative.controller.Control;
import com.example.m1p10androidnative.controller.ControlIsland;
import com.example.m1p10androidnative.utils.AccessHttp;
import com.example.m1p10androidnative.utils.AsyncResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;

public class IslandAccess implements AsyncResponse {
    private static final String IP = "http://10.200.222.125:3000";
    //private static final String IP = "https://m1p10androidnative-manoaniony-manolotsoa.onrender.com";
    //private static final String IP = "http://192.168.88.59:3000";
    private String methode = null;
    private String url = null;
    private Control control;

    private static Context context;

    private List<Island> listeIsland;
    ObjectMapper om = new ObjectMapper();
    public IslandAccess(){
        control = Control.getInstance();
    }
    @Override
    public void progessFinish(String output) {
        JsonNode jsonNode = null;
        try {
            List<Island> listeIsland = new ArrayList<>();
            jsonNode = om.readTree(output);
            JsonNode data = jsonNode.get("data");
            int type = jsonNode.get("type").asInt();
            if(type == 1){
                if(data.size() > 0){
                    for (int i=0;i<data.size();i++ ) {
                        Island island = new Island(data.get(i).get("nomIle").asText(),data.get(i).get("img").asText(),data.get(i).get("description").asText());
                        listeIsland.add(island);
                    }
                    control.setListeIsland(listeIsland);
                }
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
    public void getAllIsland(){
        methode = "GET";
        url = Configuration.getBaseUrl()+"/test/getAllIsland";
        AccessHttp accessDonne = new AccessHttp(methode);
        accessDonne.delegate = this;
        accessDonne.execute(url);
        //RequestAccess.delegate = this;
        //RequestAccess.get(context,url);
    }

    /*public void prendTousIles(Context context){
        List<Island> liste = new ArrayList<Island>();
        methode = "GET";
        url = IP+"/test/getAllIsland";
        RequestAccess.delegate = this;
        RequestAccess.get(context,url);
    }*/
}
