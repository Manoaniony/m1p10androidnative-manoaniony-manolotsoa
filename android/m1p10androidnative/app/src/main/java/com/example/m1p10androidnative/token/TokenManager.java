package com.example.m1p10androidnative.token;

import android.app.Application;

public class TokenManager {
    private static TokenManager instance;
    private String authToken;

    private TokenManager() {
        // Private constructor to enforce singleton pattern
    }

    public static synchronized TokenManager getInstance() {
        if (instance == null) {
            instance = new TokenManager();
        }
        return instance;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String token) {
        authToken = token;
    }
}
