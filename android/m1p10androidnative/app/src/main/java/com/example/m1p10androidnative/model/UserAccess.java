package com.example.m1p10androidnative.model;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.example.m1p10androidnative.constants.Configuration;
import com.example.m1p10androidnative.controller.Control;
import com.example.m1p10androidnative.utils.AccessHttp;
import com.example.m1p10androidnative.utils.AsyncResponse;
import com.example.m1p10androidnative.utils.SharedPreferencesUtil;
import com.example.m1p10androidnative.view.LoadingActivity;
import com.example.m1p10androidnative.view.LoginActivity;
import com.example.m1p10androidnative.view.MainActivity;
import com.example.m1p10androidnative.view.SignInActivity;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class UserAccess implements AsyncResponse {

    //private static final String IP = "http://192.168.88.59:3000";
    private static final String IP = "http://10.200.222.125:3000";

    private String methode = null;
    private String url = null;
    private Control control;

    private static Context context;
    ObjectMapper om = new ObjectMapper();

    public UserAccess(Context context) {
        this.context = context;
        control = Control.getInstance();
    }

    @Override
    public void progessFinish(String output) {
        JsonNode jsonNode = null;
        try {
            Log.d("message", "++++" + output);
            jsonNode = om.readTree(output);
            JsonNode data = jsonNode.get("data");
            int type = jsonNode.get("type").asInt();
            if (type == 1) {
                SharedPreferencesUtil.writeString(context, "mail", data.get("mail").asText());
                SharedPreferencesUtil.writeString(context, "nom", data.get("nom").asText());
                SharedPreferencesUtil.writeString(context, "prenom", data.get("prenom").asText());
                Intent intent = new Intent(context, LoadingActivity.class);
                context.startActivity(intent);
                ((LoginActivity) context).finish();
            } else {
                if (type == 2) {
                    Toast.makeText(context, "L'insertion est un succès.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, LoginActivity.class);
                    context.startActivity(intent);
                    ((SignInActivity) context).finish();
                } else {
                    Toast.makeText(context, "Vérifiez votre email et votre mot de passe", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public void ajouterUsers(Users user) {
        methode = "POST";
        url = Configuration.getBaseUrl() + "/users/ajoutUsers";
        AccessHttp accessDonne = new AccessHttp(methode);
        accessDonne.delegate = this;
        try {
            accessDonne.addParam("data", om.writeValueAsString(user));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        accessDonne.execute(url);
    }

    public void login(String mail, String mdp, Context context) {
        this.context = context;
        methode = "POST";
        Map<String, String> donne = new HashMap<>();
        url = Configuration.getBaseUrl() + "/auth/login";
        donne.put("mail", mail);
        donne.put("mdp", mdp);
        AccessHttp accessDonne = new AccessHttp(methode);
        accessDonne.delegate = this;
        try {
            accessDonne.addParam("data", om.writeValueAsString(donne));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        accessDonne.execute(url);
    }
}
