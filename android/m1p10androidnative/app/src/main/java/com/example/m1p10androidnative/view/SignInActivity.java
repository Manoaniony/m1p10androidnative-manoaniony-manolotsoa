package com.example.m1p10androidnative.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.m1p10androidnative.R;
import com.example.m1p10androidnative.application.MyApplication;
import com.example.m1p10androidnative.constants.Configuration;
import com.example.m1p10androidnative.controller.Control;
import com.example.m1p10androidnative.fetcher.FetchData;
import com.example.m1p10androidnative.fetcher.OnDataFetchedListener;
import com.example.m1p10androidnative.provider.TokenProvider;
import com.example.m1p10androidnative.thread.ToastThreading;
import com.example.m1p10androidnative.token.TokenManager;

import org.json.JSONException;
import org.json.JSONObject;

public class SignInActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        init();
    }

    private EditText txtNom;
    private EditText txtPrenom;
    private EditText txtAdresse;
    private EditText txtPhone;
    private EditText txtCin;
    private EditText txtMail;
    private EditText txtMdp;
    private Control control;


    private void init() {
        txtNom = (EditText) findViewById(R.id.txtNom);
        txtPrenom = (EditText) findViewById(R.id.txtPrenom);
        txtAdresse = (EditText) findViewById(R.id.txtAdresse);
        txtPhone = (EditText) findViewById(R.id.txtPhone);
        txtCin = (EditText) findViewById(R.id.txtCIN);
        txtMail = (EditText) findViewById(R.id.txtMail);
        txtMdp = (EditText) findViewById(R.id.txtMdp);
        ecouteCalcul();
        goToLogin();
    }

    private void ecouteCalcul() {
        ((Button) findViewById(R.id.btnEnreg)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String nom = null;
                String prenom = null;
                String adresse = null;
                String phone = null;
                String cin = null;
                String mail = null;
                String mdp = null;
                try {
                    nom = txtNom.getText().toString();
                    prenom = txtPrenom.getText().toString();
                    adresse = txtAdresse.getText().toString();
                    phone = txtPhone.getText().toString();
                    cin = txtCin.getText().toString();
                    mail = txtMail.getText().toString();
                    mdp = txtMdp.getText().toString();

                    JSONObject body = new JSONObject();
                    body.put("nom", nom);
                    body.put("prenom", prenom);
                    body.put("adresse", adresse);
                    body.put("phone", phone);
                    body.put("cin", cin);
                    body.put("mail", mail);
                    body.put("mdp", mdp);
                    FetchData.makePostRequest(Configuration.getBaseUrl() + "/users/ajoutUsers", body, new OnDataFetchedListener() {
                        @Override
                        public void onDataFetched(JSONObject data) {
                            try {
                                String type = data.getString("type");
                                if (Integer.parseInt(type) == 2) {
                                    ToastThreading toastThreadingSuccess = new ToastThreading(SignInActivity.this);
                                    toastThreadingSuccess.setMessage("L'insertion est un succès.");
                                    toastThreadingSuccess.start();

                                    Intent intent;
                                    intent = new Intent(SignInActivity.this, LoginActivity.class);
                                    startActivity(intent);
                                    finish();
                                }
                            } catch (JSONException e) {
                                ToastThreading toastThreadingError = new ToastThreading(SignInActivity.this);
                                toastThreadingError.setMessage(e.getMessage());
                                toastThreadingError.start();
                            }
                        }

                        @Override
                        public void onError(String errorMessage) {
                            Toast.makeText(SignInActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void ajout(String nom, String prenom, String adresse, String cin, String phone, String mail, String mdp) {
        this.control.creeUser(this, nom, prenom, adresse, cin, phone, mail, mdp);
    }

    private void goToLogin() {
        ((TextView) findViewById(R.id.goToLogin)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(SignInActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}