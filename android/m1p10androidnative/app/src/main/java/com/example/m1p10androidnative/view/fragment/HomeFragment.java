package com.example.m1p10androidnative.view.fragment;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.FragmentManager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.m1p10androidnative.R;
import com.example.m1p10androidnative.application.MyApplication;
import com.example.m1p10androidnative.constants.Configuration;
import com.example.m1p10androidnative.controller.Control;
import com.example.m1p10androidnative.fetcher.FetchData;
import com.example.m1p10androidnative.fetcher.OnDataFetchedListener;
import com.example.m1p10androidnative.model.Island;
import com.example.m1p10androidnative.thread.ListIslandThreading;
import com.example.m1p10androidnative.view.ListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {
    private Control control;
    private Context context;
    private SearchView searchView;
    private FragmentManager fragmentManager;

    public HomeFragment(Context context) {
        this.context = context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        init(view);
        creationList(view);
        return view;
    }

    private void init(View view) {
//        control = Control.getInstance();
        FragmentManager fragmentManager = getParentFragmentManager();
        searchView = view.findViewById(R.id.idSearch);
    }

    private void creationList(View view) {
        MyApplication myApplication = (MyApplication) getActivity().getApplication();
        List<Island> listeIsland = myApplication.getListIsland();
        /*get list island*/
        if (listeIsland != null) {
            ListAdapter adapter = new ListAdapter(context, listeIsland);
            ListView list = view.findViewById(R.id.idListView);
            list.setAdapter(adapter);
            list.setClickable(true);
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    searchList(listeIsland, newText, adapter);
                    //adapter.getFilter().filter(newText);
                    return true;
                }
            });
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    someMethodInFragment(context, listeIsland.get(i));
                }
            });
        }
    }

    public void searchList(List<Island> liste, String text, ListAdapter adapter) {
        List<Island> searchList = new ArrayList<Island>();
        for (Island dataClass : liste) {
            if (dataClass.getNomIle().toLowerCase().contains(text.toLowerCase())) {
                searchList.add(dataClass);
            }
        }
        adapter.searchDataList(searchList);
    }

    public void someMethodInFragment(Context context, Island arg) {
        FragmentManager fragmentManager = getParentFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        DetailsFragment yetAnotherFragment = new DetailsFragment(context, arg);
        fragmentTransaction.add(R.id.fragment_container, yetAnotherFragment);
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment_container);
        if (fragment != null) {
            fragmentTransaction.remove(fragment);
        }
        fragmentTransaction.commit();
    }
}