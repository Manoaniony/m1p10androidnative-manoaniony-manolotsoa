package com.example.m1p10androidnative.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.m1p10androidnative.R;
import com.example.m1p10androidnative.model.Island;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

    public class ListAdapter extends ArrayAdapter<Island> {

    private List<Island> liste;

    public ListAdapter(@NonNull Context context, List<Island> resource) {
        super(context, 0,resource);
        this.liste = resource;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        Island island = getItem(i);
        if(view == null){
            view = LayoutInflater.from(getContext()).inflate(R.layout.list_item,viewGroup,false);
        }
        TextView nomIle = (TextView) view.findViewById(R.id.listName);
        //ImageView image = (ImageView) view.findViewById(R.id.imgBtnIsland);
        nomIle.setText(island.getNomIle());
        return view;
    }

    public void searchDataList(List<Island> searchList){
        liste = searchList;
        notifyDataSetChanged();
    }
}
