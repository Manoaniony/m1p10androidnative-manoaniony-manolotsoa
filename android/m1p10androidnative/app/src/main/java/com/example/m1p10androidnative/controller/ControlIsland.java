package com.example.m1p10androidnative.controller;

import com.example.m1p10androidnative.model.Island;
import com.example.m1p10androidnative.model.IslandAccess;

import java.util.List;

public class ControlIsland {

    private static ControlIsland instance = null ;
    private static IslandAccess access;
    private List<Island> listeIsland;

    public static final ControlIsland getInstance(){
        if(ControlIsland.instance == null){
            ControlIsland.instance = new ControlIsland();
            access = new IslandAccess();
            access.getAllIsland();
            //access.prendTousIles(context);
        }
        return ControlIsland.instance;
    }

    public List<Island> getListeIsland() {
        return listeIsland;
    }

    public void setListeIsland(List<Island> listeIsland) {
        this.listeIsland = listeIsland;
    }
}
