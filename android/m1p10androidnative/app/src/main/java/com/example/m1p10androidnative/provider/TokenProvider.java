package com.example.m1p10androidnative.provider;

import com.example.m1p10androidnative.application.MyApplication;
import com.example.m1p10androidnative.token.TokenManager;

public class TokenProvider {
    private MyApplication myApplication;

    public TokenProvider(MyApplication myApplication) {
        this.myApplication = myApplication;
    }

    public String getTokenAuth(){
        TokenManager tokenManager = myApplication.getTokenManager();
        return tokenManager.getAuthToken();
    }

    public void setTokenAuth(String token){
        TokenManager tokenManager = myApplication.getTokenManager();
        tokenManager.setAuthToken(token);
    }

}
