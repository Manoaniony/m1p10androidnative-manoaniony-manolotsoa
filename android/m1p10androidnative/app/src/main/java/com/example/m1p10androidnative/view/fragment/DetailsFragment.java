package com.example.m1p10androidnative.view.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.m1p10androidnative.R;
import com.example.m1p10androidnative.model.Island;
import com.example.m1p10androidnative.utils.AssetImageLoader;

public class DetailsFragment extends Fragment {
    private Island isl;
    private TextView nomIle;
    private ImageView img;
    private TextView desc;

    private Context context;
    public DetailsFragment(Context context,Island isl) {
        this.context = context;
        this.isl = isl;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        init(view);
        nomIle.setText(isl.getNomIle());
        desc.setText(isl.getDescription());

        AssetImageLoader assetImageLoader = new AssetImageLoader(context);
        Bitmap bitmap = assetImageLoader.loadBitmapFromAssets(isl.getImg());
        img.setImageBitmap(bitmap);
        return view;
    }
    private void init(View view){
        nomIle = view.findViewById(R.id.idDtlsNom);
        desc = view.findViewById(R.id.idDtlsDesc);
        img = view.findViewById(R.id.idDtlsImg);
    }
}