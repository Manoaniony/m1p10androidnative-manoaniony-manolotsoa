package com.example.m1p10androidnative.model;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class Island implements Serializable {
    private String nomIle;
    private String img;
    private String description;

    public Island(String nomIle, String img, String description) {
        this.nomIle = nomIle;
        this.img = img;
        this.description = description;
    }

    public String getNomIle() {
        return nomIle;
    }

    public String getImg() {
        return img;
    }

    public String getDescription() {
        return description;
    }

}
