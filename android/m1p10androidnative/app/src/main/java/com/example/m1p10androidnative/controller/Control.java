package com.example.m1p10androidnative.controller;

import android.content.Context;

import com.example.m1p10androidnative.model.Island;
import com.example.m1p10androidnative.model.IslandAccess;
import com.example.m1p10androidnative.model.UserAccess;
import com.example.m1p10androidnative.model.Users;

import java.util.List;

public class Control {
    private static Control instance = null ;
    private UserAccess access;
    private static IslandAccess islAccess;
    private List<Island> listeIsland;

    public static final Control getInstance(){
        if(Control.instance == null){
            Control.instance = new Control();
            islAccess = new IslandAccess();
            islAccess.getAllIsland();
        }
        return Control.instance;
    }

    public void creeUser(Context context,String nom,String prenom,String adresse,String cin,String phone,String mail,String mdp){
        Users user = new Users(nom,prenom,adresse,cin,phone,mail,mdp);
        access = new UserAccess(context);
        access.ajouterUsers(user);
    }

    public void login(String mail,String mdp,Context context){
        access = new UserAccess(context);
        access.login(mail,mdp,context);
    }

    public List<Island> getListeIsland() {
        return listeIsland;
    }

    public void setListeIsland(List<Island> listeIsland) {
        this.listeIsland = listeIsland;
    }
}
