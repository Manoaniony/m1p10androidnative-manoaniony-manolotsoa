package com.example.m1p10androidnative.thread;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.m1p10androidnative.R;
import com.example.m1p10androidnative.constants.Configuration;
import com.example.m1p10androidnative.fetcher.FetchData;
import com.example.m1p10androidnative.fetcher.OnDataFetchedListener;
import com.example.m1p10androidnative.model.Island;
import com.example.m1p10androidnative.view.ListAdapter;
import com.example.m1p10androidnative.view.fragment.DetailsFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class ListIslandThreading extends Thread {
    private Context context;
    List<Island> listeIsland;
    View view;
    Fragment fragment;

    public ListIslandThreading(Context context, List<Island> listeIsland, View view, Fragment fragment) {
        this.context = context;
        this.listeIsland = listeIsland;
        this.view = view;
        this.fragment = fragment;
    }

    @Override
    public void run() {
        // Perform background work here

        // Switch to the main UI thread to display the toast
        ((Activity) context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FetchData.makeGetRequest(Configuration.getBaseUrl() + "/test/getAllIsland", new OnDataFetchedListener() {
                    @Override
                    public void onDataFetched(JSONObject data) {

                        try {
                            JSONArray jsonArray = data.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject itemObject = jsonArray.getJSONObject(i);
                                String nomIle = itemObject.getString("nomIle");
                                String img = itemObject.getString("img");
                                String description = itemObject.getString("description");

                                Island item = new Island(nomIle, img, description);
                                listeIsland.add(item);
                            }

                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    @Override
                    public void onError(String errorMessage) {

                    }
                });
                /*get list island*/
                if (listeIsland != null) {
                    ListAdapter adapter = new ListAdapter(context, listeIsland);
                    ListView list = view.findViewById(R.id.idListView);
                    list.setAdapter(adapter);
                    list.setClickable(true);
                    /*searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            searchList(listeIsland, newText, adapter);
                            //adapter.getFilter().filter(newText);
                            return true;
                        }
                    });*/
                    list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            someMethodInFragment(context, listeIsland.get(i));
                        }
                    });
                }
            }
        });
    }

    public void someMethodInFragment(Context context, Island arg) {
        FragmentManager fragmentManager = fragment.getParentFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        DetailsFragment yetAnotherFragment = new DetailsFragment(context, arg);
        fragmentTransaction.add(R.id.fragment_container, yetAnotherFragment);
        Fragment fragment = fragmentManager.findFragmentById(R.id.fragment_container);
        if (fragment != null) {
            fragmentTransaction.remove(fragment);
        }
        fragmentTransaction.commit();
    }
}
