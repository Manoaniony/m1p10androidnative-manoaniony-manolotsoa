package com.example.m1p10androidnative.view;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.example.m1p10androidnative.R;
import com.example.m1p10androidnative.controller.Control;

public class LoadingActivity extends AppCompatActivity {

    private View loadingLayout;
    //@SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        control = Control.getInstance();
        setContentView(R.layout.layout_loading);
        loadingLayout = (View) findViewById(R.id.idLyLoad);
        loadDataInBackground();
    }
    private void loadDataInBackground() {
        // Simulate data loading in the background with a delay using Handler
        new Handler().postDelayed(() -> {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
            loadingLayout.setVisibility(View.GONE);
        }, 2000);
    }
}