package com.example.m1p10androidnative.utils;

import android.os.AsyncTask;

import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.Header;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.HttpResponse;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.NameValuePair;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.ClientProtocolException;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.HttpClient;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.entity.UrlEncodedFormEntity;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.methods.HttpGet;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.methods.HttpPost;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.client.methods.HttpUriRequest;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.impl.client.DefaultHttpClient;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.message.BasicNameValuePair;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.params.HttpParams;
import com.google.firebase.crashlytics.buildtools.reloc.org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class AccessHttp extends AsyncTask<String,Integer , Long> {
    private ArrayList<NameValuePair> parametres;
    private String retour = null;
    public AsyncResponse delegate = null ;
    private String methode;

    public AccessHttp (String methode){
        this.methode = methode;
        parametres = new ArrayList<NameValuePair>();
    }
    public void addParam(String nom,String valeur){
        parametres.add(new BasicNameValuePair(nom,valeur));
    }

    @Override
    protected Long doInBackground(String... strings) {
        HttpClient cnxHttp = new DefaultHttpClient();
        HttpUriRequest paramCnx = null;
        if(methode.compareTo("GET") == 0){
            paramCnx = new HttpGet(strings[0]);
        }if(methode.compareTo("POST") == 0){
            paramCnx = new HttpPost(strings[0]);
            if(parametres != null || parametres.size() > 0){
                try {
                    ((HttpPost)paramCnx).setEntity(new UrlEncodedFormEntity(parametres));
                } catch (UnsupportedEncodingException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        try{
            HttpResponse response = cnxHttp.execute(paramCnx);
            retour = EntityUtils.toString(response.getEntity());
        }catch (UnsupportedEncodingException e){
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Long result){
        delegate.progessFinish(retour);
    }
}
