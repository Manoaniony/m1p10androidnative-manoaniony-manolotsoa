package com.example.m1p10androidnative.model;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class Users {
    private String nom;
    private String prenom;
    private String adresse;
    private String cin;
    private String telephone;
    private String mail;
    private String mdp;

    public Users() {
    }

    public Users(String nom, String prenom, String adresse, String cin, String telephone, String mail, String mdp) {
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.cin = cin;
        this.telephone = telephone;
        this.mail = mail;
        this.mdp = mdp;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public String getCin() {
        return cin;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getMail() {
        return mail;
    }

    public String getMdp() {
        return mdp;
    }

    public JSONArray convertToJSONArray(){
        List laListe = new ArrayList<>();
        laListe.add(nom);
        laListe.add(prenom);
        laListe.add(adresse);
        laListe.add(telephone);
        laListe.add(cin);
        laListe.add(mail);
        laListe.add(mdp);
        return new JSONArray(laListe);
    }
}
