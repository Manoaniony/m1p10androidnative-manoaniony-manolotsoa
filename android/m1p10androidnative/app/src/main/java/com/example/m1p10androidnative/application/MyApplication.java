package com.example.m1p10androidnative.application;

import android.app.Application;

import com.example.m1p10androidnative.constants.Configuration;
import com.example.m1p10androidnative.fetcher.FetchData;
import com.example.m1p10androidnative.fetcher.OnDataFetchedListener;
import com.example.m1p10androidnative.model.Island;
import com.example.m1p10androidnative.token.TokenManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyApplication extends Application {
    private TokenManager tokenManager;
    private List<Island> listIsland;

    public void onCreate() {
        super.onCreate();
        // Instanciez le TokenManager ici
        this.tokenManager = TokenManager.getInstance();
        this.listIsland = new ArrayList<>();

        FetchData.makeGetRequest(Configuration.getBaseUrl() + "/test/getAllIsland", new OnDataFetchedListener() {
            @Override
            public void onDataFetched(JSONObject data) {
                try {
                    JSONArray result = data.getJSONArray("data");
                    for (int i = 0; i < result.length(); i++) {
                        JSONObject itemObject = result.getJSONObject(i);
                        String nomIle = itemObject.getString("nomIle");
                        String img = itemObject.getString("img");
                        String description = itemObject.getString("description");

                        Island item = new Island(nomIle, img, description);
                        listIsland.add(item);
                    }
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public void onError(String errorMessage) {
                throw new RuntimeException(errorMessage);
            }
        });
    }

    public TokenManager getTokenManager() {
        return tokenManager;
    }

    public List<Island> getListIsland() {
        return listIsland;
    }
}
