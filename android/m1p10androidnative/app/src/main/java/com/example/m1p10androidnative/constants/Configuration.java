package com.example.m1p10androidnative.constants;

public class Configuration {
    private static String baseUrl = "https://m1p10androidnative-manoaniony-manolotsoa.onrender.com";

    public static String getBaseUrl() {
        return baseUrl;
    }
}
